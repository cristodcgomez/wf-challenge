export interface IDepartment {
  OID: number;
  Title: string;
  Color: string;
  DepartmentParent_OID: number;
}

export class Department {
  OID = -1;
  parentOID = -1;
  Title = '';
  Color = '#000000';
  isParent = false;
  selected = false;
  nSubDepartments = 0;
  subDepartments: Array < Department > = [];

  constructor(element: IDepartment) {
    this.OID = element.OID;
    this.Title = element.Title;
    this.Color = element.Color;
    this.isParent = element.DepartmentParent_OID === null ? true : false;
    this.parentOID = !this.isParent ? element.DepartmentParent_OID : -1;
  }

  setSubdepartment(subDepartment: Department) {
    this.subDepartments = [...this.subDepartments, subDepartment];
  }
}
