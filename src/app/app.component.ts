import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { DepartmentsService } from './services/departaments.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {

  departmentContent = {
    data: null,
    selected: [],
    preSelected: [],
    loaded: false
  };

  constructor(public departmentService: DepartmentsService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      // I had not enough time to implement Bonus feature #2. This should be the start:
      // Pick query params, load the data, map & filter matching IDs and set it
      // as "selected" in the data model. With an effective render control,
      // it should send to its parent component the selected value from OnInit's item function.
      this.departmentContent.preSelected = params.getAll('ids').map(el => +el);
      this.departmentContent.data = this.departmentService.load(this.departmentContent.preSelected);
    });
  }

  getSelected(event) {
    this.departmentContent.selected = event.list;
    console.log('Result: ', this.departmentContent.selected);
  }
}
