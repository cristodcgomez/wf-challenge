import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { LIST_ANIMATION } from 'src/app/animations/list.animation';
import { Department } from 'src/app/models/department';

@Component({
  selector: 'app-tree-list',
  templateUrl: './tree-list.component.html',
  styleUrls: ['./tree-list.component.sass'],
  animations: [LIST_ANIMATION]
})
export class TreeListComponent {
  listSelected: Array < number > = [];

  @Input()
  list: Observable < Array < Department >> = null;

  @Output()
  selectedItems = new EventEmitter();

  constructor() {}

  // Manage the status of the changed child.
  setSelected(event) {
    const index = this.listSelected.findIndex(el => el === event.id);
    if (index === -1) {
      this.listSelected.push(event.id);
    } else {
      this.listSelected.splice(index, 1);
    }
    this.selectedItems.emit({
      list: this.listSelected
    });
  }
}
