import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { LIST_ANIMATION } from 'src/app/animations/list.animation';
import { Department } from 'src/app/models/department';

@Component({
  selector: 'app-item-tree',
  templateUrl: './item-tree.component.html',
  styleUrls: ['./item-tree.component.sass'],
  // I don't want Angular handling render updates in a recursive component, so I changed strategy detection to increase performance
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [LIST_ANIMATION]
})
export class ItemTreeComponent implements AfterViewInit {

  data: Department = null;
  expand = false;
  nSelected = 0;

  @Input()
  set department(input: Department) {
    if (input) {
      this.data = input;
    }
  }

  @Input()
  level = 0;

  @Output()
  selectedDep: EventEmitter < {
    id: number,
    isSelected: boolean
  } > = new EventEmitter();

  @Output()
  nSub: EventEmitter <number> = new EventEmitter();

  constructor(private cdRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    if (this.data.isParent === false) {
      this.nSub.emit(1); // If it is the last leaf, It sends a signal to it's parent to increase the number of sub-departments
    }
    // As I am emitting the number of subDepartments to their respective parents just after render,
    // I need to call to the change detector of Angular, and re-render again
    this.cdRef.detectChanges();
  }

  select() {
    this.data.selected = !this.data.selected;
    this.selectedDep.emit({
      id: this.data.OID,
      isSelected: this.data.selected
    });
  }

  // Output action to update selected departments
  sendSelected(event: {
    id: number,
    isSelected: boolean
  }) {
    if (event.isSelected) {
      this.nSelected++;
    } else {
      this.nSelected--;
    }
    this.selectedDep.emit(event);
  }

  addNSubs(event) {
    this.data.nSubDepartments += event;
    this.nSub.emit(event);
  }

  identify(index: number, item: Department) { // ForEach  track Id reference, to improve performance
    return item.OID;
  }
}
