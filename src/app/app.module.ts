import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemTreeComponent } from './components/item-tree/item-tree.component';
import { TreeListComponent } from './components/tree-list/tree-list.component';
import { ColorPipePipe } from './pipes/color-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TreeListComponent,
    ItemTreeComponent,
    ColorPipePipe
  ],
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
