import { animate, query, stagger, style, transition, trigger } from '@angular/animations';

export const LIST_ANIMATION =
  trigger('list', [
    transition('* <=> *', [ // each time the binding value changes
      query(':leave', [
        stagger(100, [
          animate('0.15s', style({
            transform: 'translateX(-100px)',
            opacity: 0
          }))
        ])
      ], {
        optional: true
      }),
      query(':enter', [
        style({
          transform: 'translateX(100px)',
          opacity: 0
        }),
        stagger(100, [
          animate('0.10s', style({
            transform: 'translateX(0)',
            opacity: 1
          }))
        ])
      ], {
        optional: true
      })
    ])
  ]);
