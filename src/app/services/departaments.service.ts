import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Department, IDepartment } from '../models/department';

@Injectable({
  providedIn: 'root'
})

export class DepartmentsService {
  departmentList: Array < Department > = [];

  constructor(public http: HttpClient) {}

  load(preSelected: Array<number> = []): Observable < Array < Department >> {
    return this.http.get('./assets/data.json').pipe(
      map((rawDepartments: Array < IDepartment > ) => rawDepartments.map(dep => new Department(dep))),
      map(
        departments => {
          this.departmentList = departments.filter(el => el.isParent); // Start storing all parent departments
          departments.filter(el => !el.isParent).forEach(department => { // Filter all child departments and find its parents
            const parentDepartment = departments.find(el => el.OID === department.parentOID);
            if (parentDepartment) {
              parentDepartment.isParent = true;
              parentDepartment.setSubdepartment(department); // spread asign call
            } else {
              console.warn(`CHALLENGE: department ${department.OID} has wrong parent`);
            }
          });
          return this.departmentList;
        }
      ),
      catchError(error => { // Just in case if there is no JSON or it has an incorrect format
        console.error('CHALLENGE: Failing getting json data, please check JSON structure or if it is present in "assets" folder');
        return EMPTY;
      })
    );
  }
}
