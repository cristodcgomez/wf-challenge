import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'colorPipe'
})
export class ColorPipePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const percentToHex = (p) => {
      const percent = Math.max(0, Math.min(100, p)); // Bound percentage
      const intValue = Math.round(percent / 100 * 255); // Map percentage to nearest integer (0 - 255)
      const hexValue = intValue.toString(16); // get the hexadecimal representation
      return hexValue.padStart(2, '0').toUpperCase(); // format with leading 0 and upper case characters
    };
    return '5px solid ' + value.Color + percentToHex(100 - (args[0] * 25)); // 25% of alpha reduction for each level
  }

}
